﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

	public GameObject TrailsGO;

	Hashtable map;
	Layout layout;

	void Awake() 
	{
		map = new Hashtable();
		Point size = new Point (0.1, 0.1);
		layout = new Layout (Layout.pointy, size, new Point(0, 0));
		//Debug.Log ("Awake: Layout = " + PrintPoint(layout.size) + ", " + PrintPoint(layout.origin));

	}

	// Use this for initialization
	void Start () {



		/*
		Hex hex = new Hex (1, -1, 0);
		Point p = Layout.HexToPixel (layout, hex);
		//Cell p = Instantiate(Resources.Load("Cell")) as Cell; 
		GameObject phgo = Instantiate(Resources.Load("Cell"), new Vector3((float)p.x, (float)p.y, 0f), Quaternion.identity) as GameObject;

		Cell ph = phgo.GetComponent<Cell> ();
		//Debug.Log("Cell amount = " + p2.Amount);

		//ph.Amount = 1f;
		Debug.Log("Cell amount = " + ph.Amount);
		*/

		//Debug.Log ("Start: Layout = " + PrintPoint(layout.size) + ", " + PrintPoint(layout.origin));

		int mapRadius = 50;
		CreateGrid (mapRadius);

		/*
		AddToMap (hex, p);

		int n = 1;
		Debug.Log("HashCode of " + n + " is " + n.GetHashCode());
		Cell test = map [hash (hex.q, hex.r)] as Cell;
		Debug.Log ("Feromonia Hexissä ("+hex.q + ","+hex.r+") = "+test.Amount+" ... hash = "+hash(hex));


		unordered_set<Hex> map;
		for (int q = -map_radius; q <= map_radius; q++) {
			int r1 = max(-map_radius, -q - map_radius);
			int r2 = min(map_radius, -q + map_radius);
			for (int r = r1; r <= r2; r++) {
				map.insert(Hex(q, r, -q-r));
			}
		}
		*/
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("Update: Layout = " + PrintPoint(layout.size) + ", " + PrintPoint(layout.origin));
	}

	void CreateGrid (int mapRadius) {
		for (int q = -mapRadius; q <= mapRadius; q++) {
			int r1 = Mathf.Max(-mapRadius, -q - mapRadius);
			int r2 = Mathf.Min(mapRadius, -q + mapRadius);
			for (int r = r1; r <= r2; r++) {
				Hex hex = new Hex (q, r, -q-r);
				Point p = Layout.HexToPixel (layout, hex);
				//Debug.Log("Layout = " + layout + ", hex = " + hex + ", Point = ("+p.x+","+p.y+")");
				GameObject cellGO = Instantiate(Resources.Load("Cell"), new Vector3((float)p.x, (float)p.y, 0f), Quaternion.identity) as GameObject;
				cellGO.transform.localEulerAngles = new Vector3(0f, 0f, Random.value * 360f);
				Cell c = cellGO.GetComponent<Cell> ();
				cellGO.transform.parent = TrailsGO.transform;
				
				AddToMap(hex, c);
			}
		}
	}

	string PrintPoint(Point p) 
	{
		return "("+p.x+","+p.y+")";
	}

	void AddToMap(Hex h, Cell p) 
	{
		//Debug.Log ("map = " + map + ", h = " + h + ", p = " + p);
		//map.Add (hash (h.q, h.r), p);
		AddToMap (h, p, map);
	}

	void AddToMap(Hex h, Cell p, Hashtable m) 
	{
		//Debug.Log ("Hash(" + h.q + "," + h.r + ") = " + hash (h.q, h.r));
		m.Add (hash (h.q, h.r), p);
	}

	public void AddPheromone(Vector3 location, Pheromone p, float pheromoneStrength) 
	{
		Hex h = FractionalHex.HexRound(Layout.PixelToHex(layout, new Point(location.x, location.y)));
		if (h != null) 
		{
			Cell c = map [hash (h.q, h.r)] as Cell;
			if (c) c.AddPheromone(p, pheromoneStrength);
		}
	}

	int hash1(Hex h) {
		return hash (h.q, h.r);
	}

	int hash1(int q, int r)
	{
		int hash = 17;
		hash = hash * 31 + q.GetHashCode();
		hash = hash * 31 + r.GetHashCode();
		return hash;

		/*
		hash<int> int_hash;
		size_t hq = int_hash(h.q);
		size_t hr = int_hash(h.r);
		return hq ^ (hr + 0x9e3779b9 + (hq << 6) + (hq >> 2));
		*/
	}

	int hash(int a, int b)
	{
		uint A = (uint)(a >= 0 ? 2 * a : -2 * a - 1);
		uint B = (uint)(b >= 0 ? 2 * b : -2 * b - 1);
		int C = (int)((A >= B ? A * A + A + B : A + B * B) / 2);
		return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;
	}
}
