﻿using UnityEngine;
using System.Collections;

public class Plant : MonoBehaviour {

	public int Food = 30;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Food <= 1) {
			Destroy (this.gameObject);
		}
	}

	void CutPiece() 
	{
		if (Food <= 1) {
			Destroy (this.gameObject);
		}
		else
		{
			Food--;
		}
	}
}
