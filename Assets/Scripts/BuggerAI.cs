﻿using UnityEngine;
using System.Collections;

public enum BuggerState
{
	Spawning = 0,
	Idle = 1,
	SearchFood = 2,
	Eat = 3, 
	SearchNest = 4
}

public class BuggerAI : MonoBehaviour {

	public int State = 0;			// 0 = spawning, 1= idle, 2 = search food, 3 = eat, 4 = search nest

	static float Speed = 0.8f;
	static float SpeedRot = 120f;
	static int Damage = 6;

	public bool Enemy = false;



	public bool GotTarget = false;
	public Vector3 TargetPoint;

	public GameObject SpriteGO;
	SpriteRenderer sr;
	AudioSource audio;

	public GameObject GridGO;
	public GameObject NestGO;
	public GameObject TargetPlant;
	public GameObject TargetEnemy;
	public GameObject Cargo;
	public Vector3 CargoLocalPosition;

	public int Hitpoints = 100;
	public int Energy = 100;

	float DetectionRange = 1f;
	float EatRange = 0.2f;

	public static float EnergyRate = 2f;
	public static float EatRate = 6f;
	public static float HitRate = 1f;
	public static float ThinkRate = 1f;
	public static float SpawnRate = 1f;

	public float EnergyProgress = 0f;
	public float EatProgress = 0f;
	public float HitProgress = 0f;
	public float ThinkProgress = 0f;
	public float SpawnProgress = 0f;

	public float PheromoneRate = 0.2f;
	public float PheromoneProgress = 0f;
	public float PheromoneStrength = 1f;
	public float PheromoneWeakeningRate = 0.1f;


	// Use this for initialization
	void Start () {

		audio = GetComponent<AudioSource> ();
		GridGO = GameObject.Find ("Ground");

		// Random facing
		float facing = Random.value * 360;
		this.transform.localEulerAngles = new Vector3(0f, 0f, facing);

		// Spawning from darkness
		SpawnProgress = SpawnRate;
		sr = SpriteGO.GetComponent<SpriteRenderer> ();
		sr.color = new Color (0f, 0f, 0f, 1f);

	}
	
	// Update is called once per frame
	void Update () {

		if (State == 0) {
			SpawnProgress -= Time.deltaTime;
			if (SpawnProgress <= 0f) {
				SpawnProgress = 0f;
				State = 2;
				RandomTarget();
				sr.sortingOrder = 1;
			}
			float c = 1f - (SpawnProgress / SpawnRate);
			sr.color = new Color (c, c, c, 1f);
		} 
		else if (State == 4) 
		{
			if (Vector3.Distance(this.transform.position, NestGO.transform.position) <= EatRange)
			{
				State = 2;
				GotTarget = false;
				ThinkStart();
				CargoRemove();
				PheromoneStrength = 1f;
			}
			else
			{
				MoveToPoint(NestGO.transform.position);
			}
			
		}
		// Got targetenemy?
		// Detecting dangers?
		// Got Cargo?
		// Got targetplant?
		else if (TargetPlant) 
		{
			if (State == 3) {
				if (EatProgress <= 0f)
				{
					Debug.Log("Plant cut!");
					State = 4;
					audio.Stop ();
					TargetPlant = null;
					CargoAdd();
					PheromoneStrength = 1f;
				}
				else 
				{
					EatProgress -= Time.deltaTime;
				}
			}
			else
			{
				if (Vector3.Distance(this.transform.position, TargetPlant.transform.position) <= EatRange)
				{
					State = 3;
					EatProgress = EatRate;
					audio.Play();
				}
				else
				{
					MoveToPoint(TargetPlant.transform.position);
				}
			}
		}
		else if (DetectPlants())
		{
			Debug.Log("Detected plants");
		}
		// Detecting nest/plants?
		// Detecting trails?
		// Detecting other buggers?
		// Got TargetPoint?
		else {
			if (GotTarget)
			{
				MoveToPoint(TargetPoint);
			}
			else if (ThinkProgress <= 0f) 
			{
				RandomTarget();
			}
			else 
			{
				ThinkProgress -= Time.deltaTime;
			}
		}

		// Leave Pheromone (needs function in the Grid class)
		//Grid.

	}

	void ThinkStart()
	{
		ThinkProgress = ThinkRate/2 + (ThinkRate/2 * Random.value);
	}
	
	void MoveToPoint(Vector3 target)
	{
		float distance = Vector3.Distance (target, this.transform.position);
		// When right next to the target
		if (distance < Speed * Time.deltaTime)
		{
			GotTarget = false;
			ThinkStart();
		}
		else 
		{
			// Unohdetaan kääntyminen ennen liikkumista for now
			Vector3 move = (target - this.transform.position) / distance * Speed * Time.deltaTime;
			this.transform.position = this.transform.position + move;
			this.transform.localEulerAngles = new Vector3(0f, 0f, Mathf.Rad2Deg * Mathf.Atan2(move.y, move.x));
		}

		if (PheromoneProgress <= 0f)
		{
			Grid g = GridGO.GetComponent<Grid>();
			Pheromone ph = (State == 4) ? Pheromone.Food : Pheromone.Nest;
			g.AddPheromone(this.transform.position, ph, PheromoneStrength);
			PheromoneProgress = PheromoneRate;
			//GridGO.GetComponent<Grid>().AddPheromone(this.transform.position, Pheromone.Nest, PheromoneStrength);
		}
		else PheromoneProgress -= Time.deltaTime;
		PheromoneStrength -= PheromoneWeakeningRate * Time.deltaTime;

	}

	void RandomTarget()
	{
		float direction = Mathf.Deg2Rad * (this.transform.localEulerAngles.z + Random.value * 90f - 45f);
		float distance = (DetectionRange / 2f) + Random.value * (DetectionRange / 2f);
		TargetPoint = this.transform.position + new Vector3 (distance * Mathf.Cos  (direction), distance * Mathf.Sin (direction), 0f);
		GotTarget = true;
	}

	bool DetectPlants()
	{
		//Debug.Log ("LayerMask = " + LayerMask.NameToLayer ("Food"));
		Collider2D[] plants = Physics2D.OverlapCircleAll (new Vector2 (this.transform.position.x, this.transform.position.y), DetectionRange, 1 << LayerMask.NameToLayer("Food"));
		//Debug.Log ("Detected " + plants.Length + " plants");
		if (plants.Length > 0) {
			float distance = 1.1f * DetectionRange;
			int closest = 0;
			for (int i = 0; i < plants.Length; i++) {
				if (Vector3.Distance(this.transform.position, plants[i].transform.position) < distance) closest = i;
			}
			TargetPlant = plants[closest].gameObject;

			return true;
		} else
			return false;
	}

	void CargoAdd() 
	{
		// Instantiate as a child
	}

	void CargoRemove()
	{
		// Destroy
	}



}
