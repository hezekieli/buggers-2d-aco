﻿using UnityEngine;
using System.Collections;

public class Nest : MonoBehaviour {

	public bool Enemy = false;
	public GameObject BuggerPrefab;

	ArrayList buggers = new ArrayList();

	public int InitialCount = 20;
	public static float SpawnRate = 0.5f;
	public float SpawnProgress = 0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
		if (InitialCount > 0) 
		{
			if (SpawnProgress <= 0f) 
			{
				AddBugger();
				SpawnProgress = SpawnRate;
				InitialCount--;
			}
			else 
			{
				SpawnProgress -= Time.deltaTime;
			}
		}

	}

	void AddBugger()
	{
		GameObject buggerGO = Instantiate (BuggerPrefab, this.transform.position, Quaternion.identity) as GameObject;
		BuggerAI bugger = buggerGO.GetComponent<BuggerAI> ();
		bugger.NestGO = this.gameObject;
		buggers.Add (bugger);

	}

	void RemoveBugger(BuggerAI b)
	{
		buggers.Remove (b);
		Destroy (b.gameObject);
	}
}
