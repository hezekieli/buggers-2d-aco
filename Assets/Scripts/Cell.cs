﻿using UnityEngine;
using System.Collections;

public enum Pheromone
{
	Nest = 0,
	Food = 1
}

public class Cell : MonoBehaviour {

	public static float OpacityMax = 0.5f;
	public static float EvaporationRate = 0.01f;

	SpriteRenderer sr;
	public float[] Pheromones = new float[2] {0f, 0f};

	/*
	public float Pheromones
	{
		get 
		{
			return _Pheromones;
		}
		set 
		{
			if (value > 1) _Pheromones = 1;
			else if (value < 0) _Pheromones = 0;
			else _Pheromones = value;
		}
	}
	*/

	// Use this for initialization
	void Start () {

		//Pheromones[0] = Random.value;
		//Pheromones[1] = Random.value;
		sr = GetComponent<SpriteRenderer>();
		SetColor ();
	}
	
	// Update is called once per frame
	void Update () {
	
		for (int i = 0; i<2; i++) {
			if (Pheromones[i] > EvaporationRate*Time.deltaTime) Pheromones [i] -= EvaporationRate*Time.deltaTime;
			else Pheromones[i] = 0f;
		}
		if ((Pheromones [1] + Pheromones [0]) % (20*EvaporationRate*Time.deltaTime) > 17*EvaporationRate*Time.deltaTime)
			SetColor ();
	}

	void SetColor() 
	{
		if (sr)
		{
			float red = ((Pheromones[(int)Pheromone.Nest] > 1f) ? 1f : Pheromones[0]) * 1f;
			float green = (Pheromones[(int)Pheromone.Food] > 1f) ? 1f : Pheromones[1];
			float blue = (Pheromones[(int)Pheromone.Nest] > 1f) ? 1f : Pheromones[0];
			float opacity = 0.5f * (Pheromones[(int)Pheromone.Food]+Pheromones[(int)Pheromone.Nest]) * OpacityMax;
			sr.color = new Color(red, green, blue, opacity);
		}
	}

	public void AddPheromone(Pheromone p, float amount) 
	{
		Pheromones [(int)p] += amount;
		SetColor ();
	}

	public void SetPheromoneRandom()
	{
		Pheromones[0] = Random.value;
		Pheromones[1] = Random.value;
		sr = GetComponent<SpriteRenderer>();
		SetColor ();
	}
}
